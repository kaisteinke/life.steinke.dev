const createBtn = document.getElementById("create");
const copyBtn = document.getElementById("copy")
const birthdayElem = document.getElementById("birthday");
const deathdayElem = document.getElementById("death");
const grid = document.getElementById("grid");

createBtn.addEventListener("click", () => {
  // clear existing elements
  grid.innerHTML = "";
  // set dates
  const birthday = new Date(birthdayElem.value);
  const deathday = addYears(birthday, deathdayElem.value)
  // weeks
  const weeks = parseInt(weeksBetween(birthday, deathday));
  const completedWeeks = parseInt(weeksBetween(birthday, new Date()));
  // row which will be filled with element later
  let row;
  // go through all weeks
  for(let i = 0; i < weeks; i++){
    // determine week shape
    let shape = (i < completedWeeks) ? "full" : "empty";
    // if there should be a new row
    if(i % 52 === 0 || i === 0){
      // append row to grid
      row = document.createElement("div");
      row.className = "row";
      grid.appendChild(row);
    }
    // create shapes
    createBlock(shape, row);
  }
});

// copy to clipboard logic using html2canvas
copyBtn.addEventListener("click", () => {
  document.getElementById("loadergroup").style = "visibility:visible;display:block;"
  html2canvas(document.querySelector("#grid")).then(canvas => {
    canvas.toBlob(blob => {
      const item = new ClipboardItem({ "image/png": blob });
      navigator.clipboard.write([item]);
      document.getElementById("loadergroup").style = ""
    })
  });
});

function getWeekNumber(d) {
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
  var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
  return weekNo;
}

function addYears(date, years) {
  let tempdate = new Date(date)
  tempdate.setFullYear(date.getFullYear() + parseInt(years));
  return tempdate;
}

function weeksBetween(d1, d2) {
  return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
}

function createBlock(shape, parent){
  const block = document.createElement("div");
  block.classList.add("block", shape);
  parent.appendChild(block);
}